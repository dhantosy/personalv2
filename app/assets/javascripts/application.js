// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require foundation
//= require turbolinks
//= require_tree .

$(function(){ $(document).foundation()
  .ready(function(){
    $(window).bind('scroll',function(e){
        parallaxScroll();
      });

      function parallaxScroll(){
        var scrolledY = $(window).scrollTop();
      $('.footer-hero').css('background-position','center -'+((scrolledY*0.3))+'px');
      $('.banner-hero').css('background-position','center -'+((scrolledY*0.3))+'px');

      }
  });
});

$(".accordion dd").on("click", "a:eq(0)", function (event)
  {
    var dd_parent = $(this).parent();

      if(dd_parent.hasClass('active'))
        $(".accordion dd div.content:visible").slideToggle("normal");
      else
      {
        $(".accordion dd div.content:visible").slideToggle("normal");
        $(this).parent().find(".content").slideToggle("normal");
      }
  });

$(".arrow").click(function(e){
    e.preventDefault();
    $('body,html').stop(true,true).animate({scrollTop:
      $("#info").offset().top
    }, 750);
});